const content = document.getElementById('content');
content.innerHTML = loading();

function loading() {
    return `<tr>
    <td colspan="6" class="text-center">Loading...</td>
  </tr>`;
}

fetch('https://jsonplaceholder.typicode.com/users')
    .then((result) => result.json())
    .then((result) => {
        content.innerHTML = render(result);
    })
    .catch((error) => (content.innerHTML = error));

function render(result) {
    let table;
    result.forEach((data) => {
        table += `<tr>
                <td>${data.id}</td>
                <td>${data.name}</td>
                <td>${data.username}</td>
                <td>${data.email}</td>
                <td>
                  ${data.address.street},
                  ${data.address.suite}, 
                  ${data.address.city}
                 </td>
                <td>${data.company.name}</td>
              </tr>`;
    });
    return table;
}